# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.8.0](https://gitlab.com/biomedit/gpg-lite/compare/0.7.2...0.8.0) (2021-04-13)


### ⚠ BREAKING CHANGES

* GPGStore.export() now directly returns a bytes object instead of a file object

* GPGStore.export(): Change return type to bytes ([49945ea](https://gitlab.com/biomedit/gpg-lite/commit/49945ead0ab7a784dcd99874c0bcdae4ec386e0d))

### [0.7.2](https://gitlab.com/biomedit/gpg-lite/compare/0.7.1...0.7.2) (2020-11-23)


### Features

* **encrypt:** add compression algorithm and level options ([393932c](https://gitlab.com/biomedit/gpg-lite/commit/393932c8e2911d2c3196a8c17a52621ca7ebaec1)), closes [#16](https://gitlab.com/biomedit/gpg-lite/issues/16)


### Bug Fixes

* **cmd:** re-raise exceptions from child threads ([f894c8b](https://gitlab.com/biomedit/gpg-lite/commit/f894c8bea8f035b1986d23de622d35155460d213)), closes [#15](https://gitlab.com/biomedit/gpg-lite/issues/15)
* **Uid:** allow missing name or email in key UID ([0a037d7](https://gitlab.com/biomedit/gpg-lite/commit/0a037d78fe8e6a9f2ef69b571b708f5350718aa4)), closes [#17](https://gitlab.com/biomedit/gpg-lite/issues/17)

### [0.7.1](https://gitlab.com/biomedit/gpg-lite/compare/0.7.0...0.7.1) (2020-10-26)

## [0.7.0](https://gitlab.com/biomedit/gpg-lite/compare/0.6.13...0.7.0) (2020-10-23)


### ⚠ BREAKING CHANGES

* interfaces to encrypt and decrypt changed

    decrypt is a regular function (instead of a context manager)
    returning signee's key fingerprints
    encrypt returns None

### Features

* encrypt and decrypt accept callables for input and output ([87fc5f8](https://gitlab.com/biomedit/gpg-lite/commit/87fc5f8a20423803ea7bd378b6cb19addf95d031))


### Bug Fixes

* **deserialize:** use correct import for collections.abc ([176807d](https://gitlab.com/biomedit/gpg-lite/commit/176807d044140c63f9e1f8295a528457151df234))

### [0.6.13](https://gitlab.com/biomedit/gpg-lite/compare/0.6.12...0.6.13) (2020-08-07)


### Bug Fixes

* **packaging:** Add py.typed ([756cb96](https://gitlab.com/biomedit/gpg-lite/commit/756cb964878c114cf39a1cddefdcdc0fba2f7af5))

### [0.6.12](https://gitlab.com/biomedit/gpg-lite/compare/0.6.11...0.6.12) (2020-08-07)


### Features

* **extract_key_id:** Improve verbosity for exceptions ([a4db24e](https://gitlab.com/biomedit/gpg-lite/commit/a4db24eb85c013004cd4c625eff143696b8046a1))


### Bug Fixes

* **GPGStore.verify, GPGStore.decrypt:** preserve stderr for error handling ([3b328be](https://gitlab.com/biomedit/gpg-lite/commit/3b328be4409e8e7c7a75a082f861a9db1a1fca53))

### [0.6.11](https://gitlab.com/biomedit/gpg-lite/compare/0.6.10...0.6.11) (2020-08-04)


### Bug Fixes

* **extract_key_id_from_sig:** Support windows formatted ascii armored signatures ([80cee6a](https://gitlab.com/biomedit/gpg-lite/commit/80cee6a9e36795bd134b7aeb1da7a0721fdbcb59))

### [0.6.10](https://gitlab.com/biomedit/gpg-lite/compare/v0.6.9...v0.6.10) (2020-07-22)


### Features

* **extract_key_id_from_sig:** Support for ascii armored signatures ([03387a9](https://gitlab.com/biomedit/gpg-lite/commit/03387a9a01321b889d0b9244252014b963114307))


### Bug Fixes

* **typehints:** Add Optional t keys argument of list_pub_keys, list_sec_keys ([762322e](https://gitlab.com/biomedit/gpg-lite/commit/762322ec9bd52f846faed130316dcdb371dbbdaf))

### [0.6.9](https://gitlab.com/biomedit/gpg-lite/compare/0.6.8...0.6.9) (2020-07-09)


### Features

* **extract_key_id:** Support detached signatures ([d35e589](https://gitlab.com/biomedit/gpg-lite/commit/d35e5895030436c5ae82fb7bc5438934f36dfd2b))


### Bug Fixes

* **list_*_keys:** Make keys argument optional, return [] if keys is empty. Return all keys if keys is None ([c01e75f](https://gitlab.com/biomedit/gpg-lite/commit/c01e75f5f34253794d62f2c65187fb1dab29eea5))

### 0.6.8 (2020-07-06)


### Bug Fixes

* replace non UTF-8 characters in gpg output to avoid python decode error ([61f055e](https://gitlab.com/biomedit/gpg-lite/commit/61f055eeedd94ffb5e8db79b0ecdff239148f7f4))
* signature verification on Windows ([6e33f2f](https://gitlab.com/biomedit/gpg-lite/commit/6e33f2f6d4ba73f37348ff22ca90b7c2317b2a51))
* work around french gpg returning non utf-8 symbols ([1bd05b8](https://gitlab.com/biomedit/gpg-lite/commit/1bd05b8aab7e4b772b9f12253af771f53fee3c34))
